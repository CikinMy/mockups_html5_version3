$(document).ready(function () {

      var declaration = false; 
     
      // $('#step4').show() ;
      // $('#step1').hide()                                                                                                                                                                         ;
      $('#step1').delay(6500).fadeIn(2000);

      $('.btn-section').removeClass('active');
      var myPlayer = videojs('my-video');     

      myPlayer.on("ended", function () {
            // alert('The video has ended');
            $('.btn-section').addClass('active');
      });

      $("#login").click(function (event) {
            event.preventDefault();

            $('.main-container').hide();
            $('#step1').hide();
            $('#step2').delay(500).fadeIn(2000);
      });
      $("#btn-step2").click(function (event) {
            event.preventDefault();

            $('.main-container').hide();
            $('#step1').hide();
            $('#step2').hide();
            myPlayer.dispose();
            $('#step3').delay(500).fadeIn(2000);
      });
      $("#btn-step3").click(function (event) {
            event.preventDefault();

            $('.main-container').hide();
            $('#step1').hide();
            $('#step2').hide();
            $('#step3').hide();
            $('#step4').delay(500).fadeIn(2000);           

      });

      $("#invite").click(function (event) {
            event.preventDefault();

            $('.main-container').hide();
            $('#step1').hide();
            $('#step2').hide();
            $('#step3').hide();
            $('#step4').hide();
            $('#AddFriend').delay(500).fadeIn(2000);

      });
      $("#badge").click(function (event) {
            event.preventDefault();

            $('.main-container').hide();
            $('#step1').hide();
            $('#step2').hide();
            $('#step3').hide();
            $('#step4').hide();
            $('#badgeClaims').delay(500).fadeIn(2000);

      });
      $("#points").click(function (event) {
            event.preventDefault();

            $('.main-container').hide();
            $('#step1').hide();
            $('#step2').hide();
            $('#step3').hide();
            $('#step4').hide();
            $('#rewardsPoints').delay(500).fadeIn(2000);

      });

      $(".backToStep4").click(function (event) {
            event.preventDefault(); 
            $('#rewardsPoints').hide();  
            $('#AddFriend').hide();           
            $('#badgeClaims').hide();
            $('#step4').delay(100).fadeIn(200);

      });
      


    
     
      $('#check').click(function () {
            var name = $('#name').val();
            if (jQuery.inArray(name, names) != '-1') {
                  Label_Fadein('#status_label');
                  $('#status_label').html('The text you entered is in the array: ' + name);
                  Label_Fadeout('#status_label');
            } else {
                  Label_Fadein('#status_label');
                  $('#status_label').html('The text you entered is not in the array: ' + name);
                  Label_Fadeout('#status_label');
            }
      });//END click event

      function Label_Fadeout(a_label) {
            setTimeout(function () {
                  $(a_label).fadeOut(1600, "linear", Label_Fadeout);
                  $(a_label).val('');
            }, 2000);
      }
      function Label_Fadein(a_label) {
            setTimeout(function () {
                  $(a_label).fadeIn(60, "linear", Label_Fadeout);
                  $(a_label).val('');
            }, 60);
      }

      var friendList = [
            ["zhafri.fuad@tnb.com.my", "true"],
            ["KhairulShukeri@tnb.com.my", "false"],
            ["cikin my", "false"],
            ["norasyikin.yaacobICS@tnb.com.my", "false"],
            ["zarina", "false"],
      ];
      function setDeclaration(name, values) {
            for (var i = 0, len = values.length; i < len; i++) {
                  if (values[i][0] == name) {
                        $('#status').val(values[i][1]);
                        declaration = values[i][1];
                        break;
                  }
            }
      }

    
      $("#userInput").autocomplete({
            source: friendList.map(function (val) { return val[0] }),
            select: function (event, ui) {
                  setDeclaration(ui.item.value, friendList);
            }
      });
      
      
      
      // invite
      var enterButton = document.getElementById("enter");
      var input = document.getElementById("userInput");
      var status = document.getElementById("status");
      var ul = document.querySelector(".listItems ul");

      var item = document.getElementsByTagName("li");

      function inputLength() {
            return input.value.length;
      }

      function listLength() {
            return item.length;
      }

      function createListElement() {
            var li = document.createElement("li"); // creates an element "li"
            li.appendChild(document.createTextNode(input.value)); //makes text from input field the li text
            ul.appendChild(li); //adds li to ul
            input.value = ""; //Reset text input field
            console.log(status.value);


            //START STRIKETHROUGH
            // because it's in the function, it only adds it for new items
            function crossOut() {
                  li.classList.toggle("done");
            }

            li.addEventListener("click", crossOut);
            //END STRIKETHROUGH


            // START ADD DELETE BUTTON
            if(status.value == "true"){
                  var dBtn = document.createElement("button");
                  dBtn.innerHTML += '<i class="fas far fa-times text-danger" ></i >';
                  li.appendChild(dBtn);               
            }
            if (status.value == "false") {
                  var dBtn = document.createElement("button");
              
                  dBtn.innerHTML += '<i class="fas far fa-check text-success" ></i >';
                  li.appendChild(dBtn);                 
            }
           
            // END ADD DELETE BUTTON


            //ADD CLASS DELETE (DISPLAY: NONE)
            function deleteListItem() {
                  li.classList.add("delete")
            }
            //END ADD CLASS DELETE
      }


      function addListAfterClick() {
            if (inputLength() > 0) { //makes sure that an empty input field doesn't create a li
                  createListElement();
            }
      }

      function addListAfterKeypress(event) {
            if (inputLength() > 0 && event.which === 13) { //this now looks to see if you hit "enter"/"return"
                  //the 13 is the enter key's keycode, this could also be display by event.keyCode === 13
                  createListElement();
            }
      }


      enterButton.addEventListener("click", addListAfterClick);

      input.addEventListener("keypress", addListAfterKeypress);


// Admin Pages 
//generate lencana id


     

});


